package com.company;

/**
 * Created by lenin on 05/07/17.
 */
public class PalindromoClass {
    public boolean verificarPalindromo(String cadena){
        boolean valor=true;
        int i,ind;
        String cadena2="";
        for(int z=0;z<cadena.length(); z++){

            if(cadena.charAt(z)!=' '){
                cadena2 += cadena.charAt(z);
            }
        }
        cadena = cadena2;
        ind=cadena.length();
        for(i=0;i<(cadena.length());i++){
            if(cadena.substring(i,i+1).equals(cadena.substring(ind -1, ind)) ==false){
                valor=false;
                break;
            }
            ind --;
        }

        return valor;


    }
}
